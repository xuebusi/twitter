//
//  TwitterApp.swift
//  Twitter
//
//  Created by shiyanjun on 2023/1/22.
//

import SwiftUI

@main
struct TwitterApp: App {
    @StateObject var viewModel = AuthViewModel()
    
    var body: some Scene {
        WindowGroup {
            NavigationView {
                ContentView()
//                LoginView()
            }
            .environmentObject(viewModel)
        }
    }
}
