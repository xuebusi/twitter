//
//  Result.swift
//  Twitter
//
//  Created by shiyanjun on 2023/1/24.
//

import Foundation

struct Result {
    var code: Int
    var user: User?
}
