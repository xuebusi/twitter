//
//  User.swift
//  Twitter
//
//  Created by shiyanjun on 2023/1/24.
//

import Foundation

struct User: Codable {
    var uid: String
    var email: String
    var password: String
    var fullname: String
    var username: String
}
