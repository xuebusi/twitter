//
//  MainTabView.swift
//  Twitter
//
//  Created by shiyanjun on 2023/1/22.
//

import SwiftUI

struct MainTabView: View {
    @State private var seletedIndex = 0
    
    var body: some View {
        TabView(selection: $seletedIndex) {
            FeedView()
                .onTapGesture {
                    self.seletedIndex = 0
                }
                .tabItem {
                    Image(systemName: "house")
                }
                .tag(0)
            ExploreView()
                .onTapGesture {
                    self.seletedIndex = 1
                }
                .tabItem {
                    Image(systemName: "magnifyingglass")
                }
                .tag(1)
            NotificationsView()
                .onTapGesture {
                    self.seletedIndex = 2
                }
                .tabItem {
                    Image(systemName: "bell")
                }
                .tag(2)
            MessagesView()
                .onTapGesture {
                    self.seletedIndex = 3
                }
                .tabItem {
                    Image(systemName: "envelope")
                }
                .tag(3)
        }
    }
}

struct MainTabView_Previews: PreviewProvider {
    static var previews: some View {
        MainTabView()
    }
}
